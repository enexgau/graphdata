Experiment to see how Django can interface with graph databases.

First tried was using Neo4j and Neo4Django - BUT THIS HAS MISERABLY FAILED. Neo4Django has not had a commit for years, and only supports Django up to version 1.6. Current Django version is 2.0.5.

Might be worthwhile back tracking on the Django version (although it is now ancient).

Or perhaps better to find a different pathway?

